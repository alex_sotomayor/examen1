package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import cl.aiep.model.Conexion;

public final class DetalleVenta_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

   Conexion cnx = new Conexion();
   cnx.getConnection();
   
   ResultSet producto = cnx.cargarCombo("select ProCodigo, ProNombre from producto ");
   ResultSet grilla   = cnx.cargarCombo("select * from cw_detalle_venta ");

      out.write("    \n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Ingreso de Detalle de Venta</title>\n");
      out.write("        <link href=\"Bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <form action=\"\" method=\"\">\n");
      out.write("            \n");
      out.write("            <div class=\"form-group col-md-3\">\n");
      out.write("                <label for=\"Cantidad\">Cantidad</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"Cantidad\" class=\"form-control\" id=\"inputEmail3\">\n");
      out.write("                </div>\n");
      out.write("            </div> \n");
      out.write("            \n");
      out.write("            <div class=\"form-group col-md-6\">\n");
      out.write("                <label for=\"Producto\">Producto</label>\n");
      out.write("                <select name=\"Producto\" id=\"Venta\" size=\"1\">\n");
      out.write("                    <option value=\"0\">Seleccione Producto</option>\n");
      out.write("\n");
      out.write("                    ");

                        while (producto.next()) {
                            out.println("<option value='" + producto.getInt(1) + "'>");
                            out.println(producto.getString(2));
                            out.println("</option>");
                        }
                    
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("                <div class=\"form-group col-md-6\">\n");
      out.write("                <label for=\"descuento\">%Descuento</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"Descuento\" class=\"form-control\" id=\"inputEmail3\">\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("                \n");
      out.write("            <table border=\"1\" class=\"table table-hover\" style=\"width: 100%\">\n");
      out.write("                <thead>\n");
      out.write("                    <tr>\n");
      out.write("                        <th>Cantidad</th>\n");
      out.write("                        <th>Producto</th>\n");
      out.write("                        <th>Unitario</th>\n");
      out.write("                        <th>$Total</th>\n");
      out.write("                        <th>Quitar</th>\n");
      out.write("                    </tr>\n");
      out.write("                </thead>\n");
      out.write("\n");
      out.write("                ");

                    while (grilla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + grilla.getString(1) + "</td>");
                        out.println("<td>" + grilla.getString(2) + "</td> ");
                        out.println("<td>" + grilla.getString(3) + "</td>");
                        out.println("<td>" + grilla.getString(4) + "</td>");
                        out.println("<td><input type='button' value='Eliminar'  class='btn btn-primary' onclick='CerrarTicket'>");
                        out.println("</td>");
                        out.println("</tr>");
                    }
                
      out.write("\n");
      out.write("            </table>\n");
      out.write("              \n");
      out.write("            <div class=\"form-group col-md-4\">\n");
      out.write("                <label for=\"ValorNeto\">Valor Neto</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"ValorNeto\" class=\"form-control\" id=\"inputEmail3\">\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("            <div class=\"form-group col-md-4\">\n");
      out.write("                <label for=\"IVA\">I.V.A 19%</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"IVA\" class=\"form-control\" id=\"inputEmail3\">\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("            <div class=\"form-group col-md-4\">\n");
      out.write("                <label for=\"ValTotal\">Valor Total</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"ValorTotal\" class=\"form-control\" id=\"inputEmail3\">\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("           <div class=\"form-group col-md-6 mt-4\">\n");
      out.write("                <button type=\"submit\" name=\"Finalizar\">Finalizar</button>\n");
      out.write("            </div>  \n");
      out.write(" \n");
      out.write("            \n");
      out.write("        </form>\n");
      out.write("        <form action=\"ActualizaTicket\" method=\"POST\" id=\"formActualizar\">\n");
      out.write("            <input type=\"hidden\" id=\"IdRequerimiento\" name=\"IdRequerimiento\">\n");
      out.write("        </form>\n");
      out.write("        <script>\n");
      out.write("        function CerrarTicket( id ){\n");
      out.write("\n");
      out.write("                document.getElementById(\"IdVenta\").value = id;\n");
      out.write("                document.getElementById(\"formActualizar\").submit();\n");
      out.write("        }\n");
      out.write("        </script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
