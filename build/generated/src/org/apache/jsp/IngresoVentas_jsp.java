package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import cl.aiep.model.Conexion;

public final class IngresoVentas_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    Conexion cnx = new Conexion();
    cnx.getConnection();

    ResultSet TipoVenta = cnx.cargarCombo("select VenId, VenTipo from Venta");
    ResultSet Nombre    = cnx.cargarCombo("select CliRut, CliNombre from Cliente");

    ResultSet tabla = cnx.cargarCombo("select * from vw_vista_tabla");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Ingreso de Ventas</title>\n");
      out.write("    <link href=\"Bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <form action=\"GuardarCliente\" method=\"POST\">\n");
      out.write("\n");
      out.write("        <div class=\"form-row\">\n");
      out.write("            <div class=\"form-group col-md-3\">\n");
      out.write("                <label for=\"folio\">Folio</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"folio\" class=\"form-control\" id=\"inputEmail3\">\n");
      out.write("                </div>\n");
      out.write("            </div>     \n");
      out.write("\n");
      out.write("            <div class=\"form-group col-md-7\">\n");
      out.write("                <label for=\"Tipoventa\">Tipo de Venta</label>\n");
      out.write("                <select name=\"TipoVenta\" id=\"Venta\" size=\"1\">\n");
      out.write("\n");
      out.write("                    ");

                        while (TipoVenta.next()) {
                            out.println("<option value='" + TipoVenta.getInt(1) + "'>");
                            out.println(TipoVenta.getString(2));
                            out.println("</option>");
                        }
                    
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"form-group col-md-2\">\n");
      out.write("                <label for=\"Fecha\">Fecha Venta</label>\n");
      out.write("                <input type=\"text\" name=\"Fecha\" class=\"form-control\" id=\"inputEmail3\">\n");
      out.write("            </div> \n");
      out.write("\n");
      out.write("            <div class=\"form-group col-md-6\">  \n");
      out.write("                <label for=\"Nombre\">Nombre</label>\n");
      out.write("                <select name=\"Nombre\" id=\"Analisis\" size=\"1\">\n");
      out.write("                    <option value=\"0\">Seleccione Usuario</option>\n");
      out.write("\n");
      out.write("                    ");

                        while (Nombre.next()) {
                            out.println("<option value='" + Nombre.getInt(1) + "'>");
                            out.println(Nombre.getString(2));
                            out.println("</option>");
                        }
                    
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div> \n");
      out.write("\n");
      out.write("            <div class=\"form-group col-md-6 mt-4\">\n");
      out.write("                <button type=\"submit\" name=\"CrearVenta\">Crear Venta</button>\n");
      out.write("            </div>  \n");
      out.write("\n");
      out.write("\n");
      out.write("            <table border=\"1\" class=\"table table-hover\" style=\"width: 100%\">\n");
      out.write("                <thead>\n");
      out.write("                    <tr>\n");
      out.write("                        <th>Folio</th>\n");
      out.write("                        <th>Tipo Venta</th>\n");
      out.write("                        <th>Fecha</th>\n");
      out.write("                        <th>Nombre</th>\n");
      out.write("                    </tr>\n");
      out.write("                </thead>\n");
      out.write("\n");
      out.write("                ");

                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td>");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");
                        out.println("</tr>");
                    }
                
      out.write("\n");
      out.write("            </table>\n");
      out.write("    </form>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
