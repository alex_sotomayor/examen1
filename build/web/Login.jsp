<%-- 
    Document   : Login
    Created on : 07-sep-2019, 13:50:05
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="Bootstrap/css/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
                <div id="cuadro">
        <div id="cabeza">Bienvenido</div>
        <div id="cuerpo">
            <form class="form-horizontal" action="ValidarUsuario" method="POST">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Usuario</label>
                <div class="col-sm-10">
                    <input type="text" name="Usuario" class="form-control" id="inputEmail3" placeholder="Correo">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="Password" name="Password" class="form-control" id="inputPassword3" placeholder="Password">
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success pull-right">Ingresar</button>
                </div>
            </div>
        </form>
        </div>
        </div>
    </body>
</html>
