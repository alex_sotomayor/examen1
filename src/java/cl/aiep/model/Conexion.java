
package cl.aiep.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Conexion {
        private String server;
        private String user;
        private String base;
        private String clave;
        private int port;
        private String url;
        private Connection conexion;
        
        public Conexion(){
        this.clave  = "123456";
        this.server = "localhost";
        this.user   = "root";
        this.port   = 3306;
        this.base   = "examen1";
         
        this.url  = "jdbc:mysql://" + this.server + ":" + this.port + "/";
        this.url += this.base + "?characterEncoding=latin1";
    }
            public void getConnection() throws SQLException{
        this.conexion = null; 
        
       try{
           Class.forName("com.mysql.jdbc.Driver");
           this.conexion = (Connection) DriverManager.getConnection(  
                   this.url,  
                   this.user, 
                   this.clave 
           );
           System.out.println(" exito al conectarse ");
       }catch(ClassNotFoundException ex){
            System.out.println("Error de conexion : " + ex.getMessage());
       } 
    }
    public ResultSet EntregaDatos( String query ) throws SQLException{
        return this.conexion.createStatement().executeQuery( query );
    }
    public ResultSet cargarCombo(String query) throws SQLException{
        return this.conexion.createStatement().executeQuery( query );
    } 
    public boolean guardarDatos( String query) throws SQLException{
        return this.conexion.createStatement().execute(query );
    }
    
     public void actualizaDatos( String query) throws SQLException{
        this.conexion.createStatement().executeUpdate( query );
    }
}
