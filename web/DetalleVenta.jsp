<%-- 
    Document   : IngresoDetalle
    Created on : 07-sep-2019, 22:43:18
    Author     : Alex
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="cl.aiep.model.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
   Conexion cnx = new Conexion();
   cnx.getConnection();
   
   ResultSet producto = cnx.cargarCombo("select ProCodigo, ProNombre from producto ");
   ResultSet grilla   = cnx.cargarCombo("select * from cw_detalle_venta ");
%>    
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso de Detalle de Venta</title>
        <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <form action="" method="">
            
            <div class="form-group col-md-3">
                <label for="Cantidad">Cantidad</label>
                <div class="col-sm-10">
                    <input type="text" name="Cantidad" class="form-control" id="inputEmail3">
                </div>
            </div> 
            
            <div class="form-group col-md-6">
                <label for="Producto">Producto</label>
                <select name="Producto" id="Venta" size="1">
                    <option value="0">Seleccione Producto</option>

                    <%
                        while (producto.next()) {
                            out.println("<option value='" + producto.getInt(1) + "'>");
                            out.println(producto.getString(2));
                            out.println("</option>");
                        }
                    %>
                </select>
            </div>
            
                <div class="form-group col-md-6">
                <label for="descuento">%Descuento</label>
                <div class="col-sm-10">
                    <input type="text" name="Descuento" class="form-control" id="inputEmail3">
                </div>
            </div>
                
            <table border="1" class="table table-hover" style="width: 100%">
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Producto</th>
                        <th>Unitario</th>
                        <th>$Total</th>
                        <th>Quitar</th>
                    </tr>
                </thead>

                <%
                    while (grilla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + grilla.getString(1) + "</td>");
                        out.println("<td>" + grilla.getString(2) + "</td> ");
                        out.println("<td>" + grilla.getString(3) + "</td>");
                        out.println("<td>" + grilla.getString(4) + "</td>");
                        out.println("<td><input type='button' value='Eliminar'  class='btn btn-primary' onclick='CerrarTicket'>");
                        out.println("</td>");
                        out.println("</tr>");
                    }
                %>
            </table>
              
            <div class="form-group col-md-4">
                <label for="ValorNeto">Valor Neto</label>
                <div class="col-sm-10">
                    <input type="text" name="ValorNeto" class="form-control" id="inputEmail3">
                </div>
            </div>
            
            <div class="form-group col-md-4">
                <label for="IVA">I.V.A 19%</label>
                <div class="col-sm-10">
                    <input type="text" name="IVA" class="form-control" id="inputEmail3">
                </div>
            </div>
            
            <div class="form-group col-md-4">
                <label for="ValTotal">Valor Total</label>
                <div class="col-sm-10">
                    <input type="text" name="ValorTotal" class="form-control" id="inputEmail3">
                </div>
            </div>
            
           <div class="form-group col-md-6 mt-4">
                <button type="submit" name="Finalizar">Finalizar</button>
            </div>  
 
            
        </form>
        <form action="ActualizaTicket" method="POST" id="formActualizar">
            <input type="hidden" id="IdRequerimiento" name="IdRequerimiento">
        </form>
        <script>
        function CerrarTicket( id ){

                document.getElementById("IdVenta").value = id;
                document.getElementById("formActualizar").submit();
        }
        </script>
    </body>
</html>
